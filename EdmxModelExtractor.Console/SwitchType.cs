﻿using System;

namespace EdmxModelExtractor.Console
{
    internal enum SwitchType
    {
        Standalone,
        ParameterRequired
    }
}