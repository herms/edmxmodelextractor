﻿using System;

namespace EdmxModelExtractor.Console
{
    internal class Option
    {
        public string Switch { get; set; }
        public bool IsStandalone { get; set; }
        public string ParameterName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Option(string switchCode)
        {
            Switch = switchCode;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Option)
            {
                var option = obj as Option;
                return Switch == option.Switch;
            }
            return false;
        }
    }
}