﻿using System;
using System.Collections.Generic;

namespace EdmxModelExtractor.Console
{
    internal class Options : List<Option>
    {
        public static readonly List<Option> AvailableOptions = null;

        public static readonly Option OptionFromDatabase = new Option("d")
        {
            Name = "FromDatabase",
            IsStandalone = false,
            ParameterName = "connection_string",
            Description = "From database connection string."
        };

        public static readonly Option OptionFromEdmx = new Option("e")
        {
            Name = "FromEdmx",
            IsStandalone = false,
            ParameterName = "data",
            Description = "From EDMX file (defaults to gzipped content)."
        };

        //public static readonly Option OptionFromEdmxXml = new Option("x", SwitchType.Standalone, "",
        //    "Indicates that EDMX is in XML format (not gzipped).");

        public static readonly Option OptionMigrationId = new Option("m")
        {
            Name = "MigrationID",
            IsStandalone = false,
            ParameterName = "migration_id",
            Description = "Specify migration ID."
        };

        public static readonly Option OptionOutputPath = new Option("o")
        {
            Name = "OutputPath",
            IsStandalone = false,
            ParameterName = "path",
            Description = "Set output path. Defaults to c:\\temp."
        };

        static Options()
        {
            AvailableOptions = new Options
            {
                OptionFromDatabase,
                OptionMigrationId,
                OptionFromEdmx,
                //OptionFromEdmxXml,
                OptionOutputPath
            };
        }
    }
}