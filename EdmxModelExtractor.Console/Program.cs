﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace EdmxModelExtractor.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string outPath = Path.GetTempPath();
            string edmx = "";

            Dictionary<Option, string> options = ReadArguments(args);
            ValidateOptions(options);

            if (options.ContainsKey(Options.OptionOutputPath))
            {
                var option = Options.OptionOutputPath;
                string optionValue = GetSelectedOptionValue(options, option);
                System.Console.WriteLine(PrintSelectedOption(option, optionValue));
                // Set output path.
                outPath = Path.GetFullPath(optionValue);
                System.Console.WriteLine("Set output path to: {0}", outPath);
            }
            if (options.ContainsKey(Options.OptionFromDatabase))
            {
                if (options.ContainsKey(Options.OptionMigrationId))
                {
                    // Read from database.
                }
            }
            else if (options.ContainsKey(Options.OptionFromEdmx))
            {
                // Read from Base64 encoded gzip stream.
                var option = Options.OptionFromEdmx;
                string optionValue = GetSelectedOptionValue(options, option);
                System.Console.WriteLine(PrintSelectedOption(option, optionValue));
                string edmxPath = Path.GetFullPath(optionValue);
                string modelBase64 = File.ReadAllText(edmxPath);
                byte[] uncompressed = DecodeBase64(modelBase64);
                edmx = Encoding.UTF8.GetString(uncompressed);
                System.Console.WriteLine("Read compressed EDMX from: {0}", edmxPath);

            }

            WriteEdmxToFile(outPath, edmx);
        }

        private static string GetSelectedOptionValue(Dictionary<Option, string> options, Option option)
        {
            return options[option];
        }

        private static string PrintSelectedOption(Option option, string s)
        {
            if (!option.IsStandalone)
            {
                return PrintSelectedOption(option);
            }
            return string.Format("Option {0} selected with value {1}.", option.Name, s);
        }

        private static string PrintSelectedOption(Option option)
        {
            return string.Format("Option {0} selected.", option.Name);
        }

        private static byte[] DecodeBase64(string modelBase64)
        {
            byte[] bytes = Convert.FromBase64String(modelBase64);
            byte[] uncompressed = Decompress(bytes);
            return uncompressed;
        }

        private static void WriteEdmxToFile(string outPath, string edmx)
        {
            string now = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            string fileName = string.Format("{0}.edmx", now);
            string path = Path.Combine(outPath, fileName);

            File.WriteAllText(path, edmx);
            System.Console.WriteLine("Wrote EDMX to: {0}", path);
        }

        private static void ValidateOptions(Dictionary<Option, string> options) {}

        private static Dictionary<Option, string> ReadArguments(string[] args)
        {
            var options = new Dictionary<Option, string>();
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];
                if (arg.StartsWith("-") && arg.Length == 2)
                {
                    Option option = GetOption(arg.Substring(1, 1));
                    string optionValue = "";
                    if (!option.IsStandalone)
                    {
                        optionValue = args[++i];
                    }
                    if (options.ContainsKey(option))
                    {
                        throw new ArgumentException(string.Format("Option {0} was already selected.", option.Switch));
                    }
                    options.Add(option, optionValue);
                }
                else
                {
                    throw new ArgumentException("Argument should start with a dash.");
                }
            }
            return options;
        }

        private static Option GetOption(string optionSwitch)
        {
            Option option = Options.AvailableOptions.Find(o => o.Switch.Equals(optionSwitch));
            if (option == null)
            {
                throw new ArgumentException(string.Format("Could not find option {0}.", optionSwitch));
            }
            return option;
        }

        private void PrintUsage()
        {
            foreach (Option availableOption in  Options.AvailableOptions)
            {
                if (!availableOption.IsStandalone)
                {
                    System.Console.WriteLine("-{0} <{1}>: {2}", availableOption.Switch, availableOption.ParameterName,
                        availableOption.Description);
                }
                else
                {
                    System.Console.WriteLine("-{0}: {1}", availableOption.Switch, availableOption.Description);
                }
            }
        }

        private static byte[] Decompress(byte[] gzip)
        {
            using (var stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                const int size = 4096;
                var buffer = new byte[size];

                using (var memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    } while (count > 0);
                    return memory.ToArray();
                }
            }
        }
    }
}